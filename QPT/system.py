class QPTMasterSystem:
    def __init__(h_environment=0.8, master_comm=mpi.world, coulomb_coupling=True, xc_coupling=True, kinetic_coupling=False):
        self.h_environment = 0.8
        self.master_comm = master_comm
        self.coulomb_coupling = coulomb_coupling
        self.kinetic_coupling = kinetic_coupling
        assert kinetic_coupling == False
        self.full_system = None

    def set_systems(self, systems):
        self.systems = systems
    
    def center(self, vacuum):
        self.make_full_atoms()
        self.full_atoms.center(vacuum=0)
        raise NotImplementedError

    def make_full_atoms(self):
        atoms = Atoms()
        for system in systems:
            atoms.extend(system.atoms)
        self.full_atoms = atoms
    
    def calculate_ground_state(self):
        pass

class QPTSystem:
    def __init__(self, atoms, trajectory=None, vacuum_at_least=6):
        self.atoms = atoms # Atoms in coordinates of the full master cell
        self.offset_c = None # Offset between master and local coordinates
        self.electronic_atoms = atoms.copy() # Atoms in coordinates of the local electronic cell
        self.trajectory = trajectory
        self.vacuum_at_least = 6
 
    def get_calculator(self, **args):
        raise NotImplementedError

    def	estimate_effort(self):
        raise NotImplementedError

 

class QPTSystem:
    def __self__(self, atoms):

    def


"""
Example script for coupling two systems:

class SystemA(QPTSystem):
    def get_calculator(self, **args):
        return GPAW(mode='lcao', h=0.2, **args)

    def estimate_effort(self):
        return 32

class SystemB(QPTSystem):
    def get_calculator(self, **args):
        return GPAW(mode='lcao', h=0.2, **args)

    def estimate_effort(self):
        return 16

master = QPTMasterSystem(h_environment=0.8, master_comm=mpi.world, coulomb_coupling=True)
systemA = SystemA(read('SystemA.xyz'), trajectory='SystemA.traj', vacuum_at_least=6)
systemB = SystemB(read('SystemB.xyz'), trajectory='SystemB.traj', vacuum_at_least=6)
systemA.translate([10,10,2]))
master.set_systems([systemA, systemB])
master.center(vacuum_at_least=30)
dyn = QPTDynamics(master)
dyn.run_BOMD(1, 1000)

Example script for one system with large environment:
class System(QPTSystem):
    def get_calculator(self, **args):
        return GPAW(mode='lcao', h=0.2, **args)

    def	estimate_effort(self):
        return 32

master, system = QPT.SeparateElectronicAndEnvironment(read('system.xyz'), System, electronic_vacuum=6)
master.calculate_ground_state()
dyn = QPTDynamics(master)
dyn.run_TDDFT(1,1000, kick=[0,0,0.00001])


"""
