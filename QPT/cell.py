import numpy as np

def center_atoms_maintaining_hmod(atoms, vacuum=5, h=0.2):
    ref = atoms[0].position.copy()
    atoms.center(vacuum=vacuum)
    cell = atoms.get_cell()
    for i in range(3):
        cell[i][i] = np.ceil(cell[i][i] / (h*32)) * h*32
    atoms.set_cell(cell)
    atoms.center()
    displacement = atoms[0].position - ref
    displacement = np.round(displacement / h) * h - displacement
    atoms.translate(displacement)

def set_similar_hmod(target_atoms, make_like_atoms, h=0.2):
    refmod = target_atoms[0].position.copy() % h
    refmod2 = make_like_atoms[0].position.copy() % h
    target_atoms.translate(-refmod+refmod2)
    refmod = target_atoms[0].position.copy() % h

def plan_poisson_grids(system, h=0.2, electronic_vacuum=5):
    electronic_system = system.copy()
    center_atoms_maintaining_hmod(electronic_system, vacuum=electronic_vacuum, h=h)
    electronic_offset = np.round((system[0].position - electronic_system[0].position ) / h)
    return (system, np.array([0,0,0])), (electronic_system, electronic_offset)
