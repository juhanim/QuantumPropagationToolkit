from QPT.grid.redistribute import GeneralGrid, Redistributor

class AuxilliaryGridPoissonSolver:
    def __init__(self, gd, offset_c, poissonsolver, master_comm=None):
        self.poissonsolver = poissonsolver
        self.largegd = gd
        self.offset_c = offset_c
        self.master_comm = master_comm

    def todict(self):
        return {'name': 'AuxillaryGridPoissonSolver', 
                'poissonsolver': self.poissonsolver.todict() }
        
    def get_stencil(self):
        return self.poissonsolver.nn

    def set_grid_descriptor(self, gd):
        self.large_gg = GeneralGrid(self.largegd, self.offset_c, master_comm=self.master_comm)
        self.small_gg = GeneralGrid(gd, [0, 0, 0], master_comm=self.master_comm)
        self.poissonsolver.set_grid_descriptor(self.largegd)
        self.philarge_g = self.largegd.zeros()
        self.rholarge_g = self.largegd.zeros()
        self.rd = Redistributor(self.small_gg, self.large_gg)

    def get_description(self):
        return """AuxillaryGridPoisson solver
             Auxilliary grid size   %4d %4d %4d
             Auxilliary grid offset %4d %4d %4d
             ->%s""" % (tuple(self.largegd.N_c) + tuple(self.offset_c) + (self.poissonsolver.get_description(),))

    def initialize(self, load_gauss=False):
        self.poissonsolver.initialize(load_gauss)

    def solve(self, phi_g, rho_g, charge=None, eps=None, maxcharge=1e-6,
              zero_initial_phi=False):
        self.rd.redistribute(rho_g, self.rholarge_g)
        iters = self.poissonsolver.solve(self.philarge_g, self.rholarge_g, charge=charge, eps=eps, maxcharge=maxcharge, zero_initial_phi=zero_initial_phi)
        self.rd.redistribute(phi_g, self.philarge_g, backwards=True)
        return iters

    def estimate_memory(self, mem):
        self.poissonsolver.estimate_memory(mem)

    def __repr__(self):
        return 'AuxilliaryGridPoissonSolver(TODO)'
