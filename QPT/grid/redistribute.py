import numpy as np

from gpaw.mpi import world

debug = True

if debug:
    import svgwrite
    df = open('MPI.%03d' % world.rank,'w')

    from svgwrite import cm, mm    

    """    shapes.add(dwg.rect(insert=(5*cm, 5*cm), size=(45*mm, 45*mm), fill='blue', stroke='red', stroke_width=3))
           dwg = svgwrite.Drawing('test.svg', profile='tiny')"""

    def dbg(s):
        print >> df, s
        df.flush()
else:
    def dbg(s):
        pass

class MPIAlltoAll:
    """A helper class to handle MPI All to All requests.

       Each core must call send(rank, data) to tell they want to communicate this data.
       Also, each core must call receive(rank, size) to tell how much they wish to receive.
       In the future, it will be possible to automatically parallel transpose these sizes also.
       
       The communicate method will perform the MPI_alltoallv operation, and act as an iterator
       to handle the received data as well.

    """

    def __init__(self, comm, dtype, bufsize):
        self.comm = comm
        self.dtype = dtype

        self.buffer = np.empty( (bufsize,), dtype=dtype)
        self.send_index = 0
        self.send_sizes = np.zeros( (comm.size,), dtype=np.int)
        self.send_displacements = np.zeros( (comm.size,), dtype=np.int)

        self.recv_index = 0
        self.recv_sizes = np.zeros( (comm.size,), dtype=np.int)
        self.recv_displacements = np.zeros( (comm.size,), dtype=np.int)

    def send(self, rank, data):
        self.send_sizes[rank] = data.size
        self.send_displacements[rank] = self.send_index
        self.buffer[self.send_index:self.send_index+data.size] = data.ravel()
        self.send_index += data.size
        dbg("Sending %d items from rank %d to rank %d." % (data.size, self.comm.rank, rank))

    def receive(self, rank, size):
        self.recv_sizes[rank] = size        
        #if size != 0:
        self.recv_displacements[rank] = self.recv_index
        self.recv_index += size
        dbg("Receiving %d items from rank %d to rank %d." % (size, rank, self.comm.rank))

    def communicate(self):
        dbg("in communicate")
        # Allocate receive buffer
        recv_buffer = np.empty( (self.recv_index,), dtype=self.dtype )
        # Communicate
        #dbg("CALLING", self.buffer.shape, self.send_sizes, self.send_displacements, recv_buffer.shape, self.recv_sizes, self.recv_displacements
        self.comm.alltoallv(self.buffer, # send buffer
                            self.send_sizes,
                            self.send_displacements,
                            recv_buffer,
                            self.recv_sizes,
                            self.recv_displacements)
        #print >>self.debug, "After alltoallv"
        # Yield each received chuck in rank order
        for rank, (size, disp) in enumerate(zip(self.recv_sizes, self.recv_displacements)):
            #print >>self.debug, "Yielding", disp, disp+size
            yield recv_buffer[disp:disp+size]
        #print >>self.debug, "ALL DONE IN COMMUNICATE"

def box_size(box):
    return (box[3]-box[0])*(box[4]-box[1])*(box[5]-box[2])

def empty_box(box):
    if np.any(box == 0):
        return True
    return False

class GeneralGrid:
    def __init__(self, gd, offset_c, master_comm=None):
        self.gd = gd
        self.offset_c = np.array(offset_c)

        # Global begin of this grid
        self.gbeg_c = self.gd.beg_c + self.offset_c

        if master_comm is None and gd is not None:
            master_comm = self.gd.comm
        self.master_comm = master_comm

    def get_size(self):
        if gd is None:
            return 0
        return np.prod(gd.n_c)

    def get_bounding_box(self):
        data = np.zeros((6,), dtype=np.int)
        if self.gd is not None:
            data[:3] = self.gd.beg_c + self.offset_c
            data[3:] = self.gd.end_c + self.offset_c
        return data

    def pick_box(self, data_g, box):
        if empty_box(box):
            return np.array([])
        beg_c = self.gbeg_c
        dbg("Pick box")
        dbg("Box %d %d %d %d %d %d" % (box[0], box[1], box[2], box[3], box[4], box[5]))
        dbg("beg_c %d %d %d" % (beg_c[0], beg_c[1], beg_c[2]))
        dbg("data_g.shape %d %d %d" % data_g.shape)
        return data_g[box[0]-beg_c[0]:box[3]-beg_c[0], box[1]-beg_c[1]:box[4]-beg_c[1], box[2]-beg_c[2]:box[5]-beg_c[2]]

    def assign_box(self, target_g, box, data):
        if empty_box(box):
            return
        beg_c = self.gbeg_c
        dbg("Assign box")
        dbg("Box %d %d %d %d %d %d" % (box[0], box[1], box[2], box[3], box[4], box[5]))
        dbg("beg_c %d %d %d" % (beg_c[0], beg_c[1], beg_c[2]))
        dbg("target_g.shape %d %d %d" % target_g.shape)
        dbg("data.shape %d" % data.shape)
        target_g[box[0]-beg_c[0]:box[3]-beg_c[0], box[1]-beg_c[1]:box[4]-beg_c[1], box[2]-beg_c[2]:box[5]-beg_c[2]] = np.reshape(data, (box[3]-box[0], box[4]-box[1], box[5]-box[2]))


def intersect(bb1,bb2):
    box = np.zeros((6,), dtype=np.int)
    #print "bb1", bb1, "bb2", bb2, "from rank", world.rank
    box[:3] = np.maximum(bb1[:3], bb2[:3])
    box[3:] = np.minimum(bb1[3:], bb2[3:])
    if np.any(box[:3] >= box[3:]):
       box[:] = 0
    return box

class Redistributor:
    def __init__(self, source, target, master_comm=None):
        """Redistributes data from source to target.

           The comminicator joint_comm needs to coontain all cores which belong to either gd1, gd2 or both.
           
        """

        self.source = source
        self.target = target

        # If master_comm is not provided as an argument, use the 
        # master_comm from the grids (these must be equal)
        if master_comm is None:
            assert self.target.master_comm == self.source.master_comm
            master_comm = self.source.master_comm
        self.master_comm = master_comm

        rank = self.master_comm.rank

        # -----------------------------------------------------------
        # Phase 1: Broadcast the source and target boxes
        # -----------------------------------------------------------
        source_box = self.source.get_bounding_box()
        self.all_source_boxes = np.zeros( (6 * master_comm.size,), dtype=np.int)
        self.master_comm.all_gather(source_box, self.all_source_boxes)

        target_box = self.target.get_bounding_box()
        self.all_target_boxes = np.zeros( (6 * master_comm.size,), dtype=np.int)
        self.master_comm.all_gather(target_box, self.all_target_boxes)

        # -----------------------------------------------------------
        # Phase 2: Find out the intersections (what to send)
        # -----------------------------------------------------------
        dbg("Send intersections")
        self.intersections = np.zeros( (6 * master_comm.size,), dtype=np.int)
        for rank in range(self.master_comm.size):
            remote_target_box = self.all_target_boxes[(6*rank):(6*(rank+1))]
            intersection = intersect(source_box, remote_target_box)
            self.intersections[(rank*6):(6*(rank+1))] = intersection
            dbg("Rank %d: %d %d %d %d %d %d" % ((rank,) + tuple(intersection)))

        # -----------------------------------------------------------
        # Phase 3: Transpose intersections (what to receive)
        # -----------------------------------------------------------
        dbg("Recv intersections (transposed)")
        self.recv_intersections = np.zeros( (6 * master_comm.size,), dtype=np.int)
        self.master_comm.alltoallv(self.intersections,                         # send buffer 
                                   np.array([ 6 ] * master_comm.size),         # Send 6 numbers to each rank
                                   6 * np.array( range(master_comm.size) ),    # Send displacements
                                   self.recv_intersections,                    # receive buffer
                                   np.array([ 6 ] * master_comm.size),         # Receive 6 numbers from each rank
                                   6 * np.array( range(master_comm.size) ))    # Recveice displacements
        if debug:
            for rank in range(self.master_comm.size):
                 intersection = self.recv_intersections[(6*rank):(6*(rank+1))]
                 dbg("Rank %d: %d %d %d %d %d %d" % ((rank,) + tuple(intersection)))

    def redistribute(self, source_g, target_g, backwards=False):
        # If transfering backwards, just switch all the variables (by using temporary local variables)
        if not backwards:
            intersections, recv_intersections = self.intersections, self.recv_intersections
            target, source = self.target, self.source
        else:
            intersections, recv_intersections = self.recv_intersections, self.intersections
            source_g, target_g = target_g, source_g
            target, source = self.source, self.target

        dtype, size = np.float, 0
        if source_g is not None:
            dtype = source_g.dtype
            size = source_g.size

        # Create MPIAlltoAll helper object
        all = MPIAlltoAll(self.master_comm, dtype, size)

        # Tell it what to send
        for rank in range(self.master_comm.size):
            send_intersection = intersections[(rank*6):(6*(rank+1))]
            recv_intersection = recv_intersections[(rank*6):(6*(rank+1))]
            all.send(rank, source.pick_box(source_g, send_intersection))
            all.receive(rank, box_size(recv_intersection))

        for rank, data in enumerate(all.communicate()):
	    intersection = recv_intersections[(6*rank):(6*(rank+1))]
            dbg("Data size in redistribute %d" % data.size)
            dbg("recv_intersection %d %d %d %d %d %d" % tuple(intersection))
            dbg("box size recv_intersection %d " % box_size(intersection))
            target.assign_box(target_g, intersection, data)

