from QPT.grid.redistribute import Redistributor, GeneralGrid

from gpaw.mpi import world
from gpaw.grid_descriptor import GridDescriptor

import numpy as np

N = 32
source_decomp = np.array([2,2,2])
target_decomp = np.array([4,2,1])

source_ranks = range(np.prod(source_decomp))
target_ranks = range(np.prod(target_decomp))

source_comm = world.new_communicator(source_ranks)
target_comm = world.new_communicator(target_ranks)

offset_c = np.array( [0,0,0] )
master_comm = world

if source_comm is not None:
     source_gd = GridDescriptor([N,N,N], cell_cv=(10.0,10.0,10.0), pbc_c=[0,0,0], comm=source_comm, parsize_c=source_decomp)

     # Create global array for debugging purpose
     if source_gd.comm.rank == 0:
         source_glob_g = source_gd.zeros(global_array=True)
         for i in range(N-1):
              for j in range(N-1):
                  for k in range(N-1):
                       source_glob_g[i,j,k] = i+j*N+k*(N*N)
     else:
         source_glob_g = None

     # Parallelize the source array
     source_g = source_gd.zeros()
     source_gd.distribute(source_glob_g, source_g)
     print "Hi! Rank ", world.rank, "here. I have slice ", source_gd.beg_c, source_gd.end_c, "of size", source_g.shape, "of source array."
else:
     source_gd = None
     source_g = None

if target_comm is not None:
    target_gd = GridDescriptor([N,N,N], cell_cv=(10.0,10.0,10.0), pbc_c=[0,0,0], comm=target_comm, parsize_c=target_decomp)
    target_g = target_gd.zeros()
    print "Hi! Rank ", world.rank, "here. I have slice ", target_gd.beg_c, target_gd.end_c, "of size", target_g.shape, "of target array."
else:
    target_gd = None


source_grid = GeneralGrid(source_gd, [0,0,0], master_comm=world)
target_grid = GeneralGrid(target_gd, offset_c, master_comm=world)
rd = Redistributor(source_grid, target_grid)
rd.redistribute(source_g, target_g)
if target_gd is not None:
    target_glob_g = target_gd.collect(target_g)

if world.rank == 0:
    correct = True
    for i in range(N-1):
        for j in range(N-1):
             for k in range(N-1):
                 print i,j,k, source_glob_g[i,j,k], target_glob_g[i,j,k]  
                 if source_glob_g[i,j,k] != target_glob_g[i,j,k]:
                     correct = False
    assert correct
    

# Test backwards distribute also
source_g[:] = 0
rd.redistribute(source_g, target_g, backwards=True)

if source_gd is not None:
    source_glob_g = source_gd.collect(source_g)

if world.rank == 0:
    correct = True
    for i in range(N-offset_c[0]-1):
        for j in range(N-offset_c[1]-1):
             for k in range(N-offset_c[2]-1):
                 print i,j,k, source_glob_g[i+offset_c[0],j+offset_c[1],k+offset_c[2]], target_glob_g[i,j,k]
                 if source_glob_g[i+offset_c[0],j+offset_c[1],k+offset_c[2]] != target_glob_g[i,j,k]:
                     correct = False

    assert correct
