from QPT.grid.poisson import AuxilliaryGridPoissonSolver
from gpaw.grid_descriptor import GridDescriptor

from gpaw import GPAW, PoissonSolver, FermiDirac
from gpaw.mpi import world
from ase.io import read, write
from ase.units import Bohr

atoms = read('Na15.xyz')
atoms.center(vacuum=5)

# Original small grid
h = 0.2
N = 184
L = h*N/2 

# Large grid
N2 = 256
L2 = h*N2/2

atoms.set_cell((L,L,L))
atoms.center()

# Time with larger auxilliary grid
poissonsolver = PoissonSolver(remove_moment=9)
large_gd = GridDescriptor([N2, N2, N2], cell_cv=(L2 / Bohr,L2 / Bohr,L2 / Bohr), pbc_c=[0,0,0], comm=world, parsize_c=(4,2,4))
off = -(N2-N)//2

auxpoissonsolver = AuxilliaryGridPoissonSolver(large_gd, [off,off,off], poissonsolver, world)
calc = GPAW(mode='lcao', basis='sz(dzp)', setups={'Na':'1'}, gpts=(N//2,N//2,N//2),
            poissonsolver = auxpoissonsolver, occupations=FermiDirac(0.1))
atoms.set_calculator(calc)
atoms.get_potential_energy() 

v_Ha_aux = calc.hamiltonian.finegd.collect(calc.hamiltonian.vHt_g)
v_Ha_org = large_gd.collect(auxpoissonsolver.philarge_g)
write('aux_restricted.cube', atoms, data=v_Ha_aux)
write('aux_original.cube', atoms, data=v_Ha_org)

del calc

if 1:
    # Time with regular poisson solver
    poissonsolver = PoissonSolver(remove_moment=9)
    calc = GPAW(mode='lcao', basis='sz(dzp)', setups={'Na':'1'}, gpts=(N//2,N//2,N//2),
                poissonsolver = poissonsolver, occupations=FermiDirac(0.1))
    atoms.set_calculator(calc)
    atoms.get_potential_energy() 
    v_Ha = calc.hamiltonian.finegd.collect(calc.hamiltonian.vHt_g)
    write('real_original.cube', atoms, data=v_Ha)
    write('diff.cube', atoms, data=v_Ha_aux - v_Ha)
    write('div.cube', atoms, data=v_Ha_aux / v_Ha)
    del calc

if 1:
    # Time with huge cell, regular poisson solver
    atoms.set_cell( [L2,L2,L2])
    atoms.center()
    poissonsolver = PoissonSolver(remove_moment=9)
    calc = GPAW(mode='lcao', basis='sz(dzp)', setups={'Na':'1'}, gpts=(N2//2,N2//2,N2//2),
                poissonsolver = poissonsolver, occupations=FermiDirac(0.1))
    atoms.set_calculator(calc)
    atoms.get_potential_energy() 
    del calc
